package cliffberg.pddexample.compa;

import cliffberg.pddexample.compb.ID;
import cliffberg.pddexample.compa.proto.MergerGrpc.IdList;
import cliffberg.pddexample.compa.proto.MergerGrpc.MergeRequest;
import cliffberg.pddexample.compb.CompB;
import java.util.List;
import java.util.LinkedList;

public class MergerService extends MergerGrpc.MergerImplBase {

	@Override
	public void merge(MergeRequest mergeRequest, StreamObserver<HelloReply> responseObserver) {

		IdList primaryIds = mergeRequest.primary;
		IdList secondaryIds = mergeRequst.secondary;

		IdList.Builder idListBuilder = IdList.newBuilder();
		List<ID> primaryList = primaryIds.getIdList();
		List<ID> secondaryList = secondaryIds.getIdList();
		List<ID> newList = doMerge(primaryList, secondaryList);
		// Ref: https://developers.google.com/protocol-buffers/docs/reference/java-generated#repeated-fields

		/* Construct the gRPC reply */
		IdList reply = IdList.newBuilder().addAllId(newList).build();
		responseObserver.onNext(reply);
		responseObserver.onCompleted();
    }

	/**
	Algorithm compa-1
	*/
	private static List<ID> doMerge(List<ID> primaryList, List<ID> secondaryList) {
	    List<ID> newList = new LinkedList<ID>(primaryList); // create shallow copy of primaryList.
	    for (ID id : secondaryList) {
			CompB.addID(newList, id);
		}
	    return newList;
	}
}
